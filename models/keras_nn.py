from keras.layers import Dense, Activation, Dropout
from keras.models import Sequential
from keras.metrics import mean_squared_logarithmic_error, mean_squared_error
from keras.activations import relu
from keras.wrappers.scikit_learn import KerasRegressor

from keras.optimizers import SGD
from sklearn.model_selection import GridSearchCV


optimizer = ['rmsprop', 'adam', 'adagrad', 'sgd']
init = ['glorot_uniform', 'normal', 'uniform']
sgd_lr = [0.001, 0.01, 0.1, 0.4]
sgd_momentum = [0.0, 0.2, 0.4, 0.6, 0.8, 0.9]
epochs = [20,30, 50, 100, 150]
batches = [5, 10, 20, 30]
activation = ['relu', 'elu', 'tanh', 'sigmoid', 'linear']
layer1 = [10, 30, 70, 200, 600, 1200]
layer2 = [10, 30, 70, 200, 600, 1200]
layer3 = [10, 30, 70, 200, 600, 1200]
dropout_rate = [0.0, 0.2, 0.5]

params = dict(optimizer=optimizer,
              init=init,
              sgd_lr=sgd_lr,
              sgd_momentum=sgd_momentum,
              epochs=epochs,
              batches=batches,
              activation=activation,
              layer1=layer1, layer2=layer2, layer3=layer3, dropout_rate=dropout_rate
             )


def get_model(optimizer='adam', init='uniform', sgd_lr=0.001, sgd_momentum=0.2, epochs=10, batches=15,
              activation='relu', layer1=120, layer2=120, layer3=30, dropout_rate=0.2):

    print("Called with params ==>\n%s \n<==" % params)

    cols=73
    if params is None:
        return
    model= Sequential()
    model.add(Dense(units=layer1, input_shape=(cols, ), activation=activation ))
    model.add(Dropout(dropout_rate))
    model.add(Dense(units=layer2, activation=activation))
    model.add(Dropout(dropout_rate))
    model.add(Dense(units=layer3, activation=activation))
    model.add(Dropout(dropout_rate))
    model.add(Dense(units=1, kernel_initializer=init, activation=activation))
    if optimizer == 'SGD':
        optimizer = SGD(lr=sgd_lr, momentum=sgd_momentum)

    model.compile(optimizer=optimizer, loss='mean_squared_error', metrics=['mean_squared_error'])

    return model


def grid_search(X, y, fold5):
    regressor = KerasRegressor(build_fn=get_model)
    gs_result = GridSearchCV(regressor, param_grid=params, n_jobs=1)
    gs_result.fit(X, y)