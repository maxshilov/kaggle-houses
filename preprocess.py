from sklearn.preprocessing import LabelEncoder
from sklearn.preprocessing import StandardScaler
import pandas as pd


numerical_fields = ['MSSubClass', 'LotFrontage', 'LotArea', 'OverallQual', 'OverallCond', 'YearBuilt', 'YearRemodAdd',
                    'BsmtFinSF1', 'BsmtFinSF2', 'BsmtUnfSF',
                    'TotalBsmtSF', '1stFlrSF', '2ndFlrSF', 'LowQualFinSF', 'GrLivArea', 'BsmtFullBath', 'BsmtHalfBath',
                    'FullBath', 'HalfBath', 'BedroomAbvGr', 'KitchenAbvGr',
                    'TotRmsAbvGrd', 'Fireplaces', 'GarageYrBlt', 'GarageCars', 'GarageArea',
                    'WoodDeckSF', 'OpenPorchSF', 'EnclosedPorch', '3SsnPorch', 'ScreenPorch', 'PoolArea',
                    'MiscVal', 'MoSold', 'YrSold']

categorical_fields = ['MSZoning', 'Street',  'LotConfig',
                      'Neighborhood', 'Condition1', 'Condition2', 'BldgType', 'HouseStyle',
                      'RoofStyle', 'RoofMatl', 'Exterior1st', 'Exterior2nd',
                      'Foundation', 'Heating',  'Electrical',
                      'GarageType', 'GarageFinish', 'MiscFeature', 'SaleType', 'SaleCondition'
                      ]

REPLACE_VALUES = {'Utilities': {'AllPub': 1, 'NoSeWa': 0, 'NoSewr': 0, 'ELO': 0},
                  'Street': {'Pave': 1, 'Grvl': 0 },
                  'FireplaceQu': {'Ex': 5,
                                  'Gd': 4,
                                  'TA': 3,
                                  'Fa': 2,
                                  'Po': 1,
                                  'NoFireplace': 0,
                                  'NA': 0
                                  },
                  'Fence': {'GdPrv': 2,
                            'GdWo': 2,
                            'MnPrv': 1,
                            'MnWw': 1,
                            'NoFence': 0,
                            'NA': 0},
                  'ExterQual': {'Ex': 5,
                                'Gd': 4,
                                'TA': 3,
                                'Fa': 2,
                                'Po': 1
                                },
                  'ExterCond': {'Ex': 5,
                                'Gd': 4,
                                'TA': 3,
                                'Fa': 2,
                                'Po': 1
                                },
                  'BsmtQual': {'Ex': 5,
                               'Gd': 4,
                               'TA': 3,
                               'Fa': 2,
                               'Po': 1,
                               'NoBsmt': 0,
                               'NA': 0},
                  'BsmtExposure': {'Gd': 3,
                                   'Av': 2,
                                   'Mn': 1,
                                   'No': 0,
                                   'NoBsmt': 0,
                                   'NA': 0},
                  'BsmtCond': {'Ex': 5,
                               'Gd': 4,
                               'TA': 3,
                               'Fa': 2,
                               'Po': 1,
                               'NoBsmt': 0,
                               'NA': 0},
                  'GarageQual': {'Ex': 5,
                                 'Gd': 4,
                                 'TA': 3,
                                 'Fa': 2,
                                 'Po': 1,
                                 'NoGarage': 0,
                                 'NA':0},
                  'GarageCond': {'Ex': 5,
                                 'Gd': 4,
                                 'TA': 3,
                                 'Fa': 2,
                                 'Po': 1,
                                 'NoGarage': 0,
                                 'NA': 0},
                  'KitchenQual': {'Ex': 5,
                                  'Gd': 4,
                                  'TA': 3,
                                  'Fa': 2,
                                  'Po': 1},
                  'Functional': {'Typ': 0,
                                 'Min1': 1,
                                 'Min2': 1,
                                 'Mod': 2,
                                 'Maj1': 3,
                                 'Maj2': 4,
                                 'Sev': 5,
                                 'Sal': 6},
                  'LandContour': {'Low': 0, 'HLS': 1, 'Bnk': 2, 'Lvl': 3},
                  'LandSlope': {'Sev': 0, 'Mod': 1, 'Gtl': 2},
                  'LotShape':  {'IR3': 0, 'IR2': 1, 'IR1': 2, 'Reg': 3},
                  'BsmtFinType1': {'NA': 0, 'Unf': 1, 'LwQ': 2, 'Rec': 3, 'BLQ': 4, 'ALQ': 5, 'GLQ': 6},
                  'BsmtFinType2': {'NA': 0, 'Unf': 1, 'LwQ': 2, 'Rec': 3, 'BLQ': 4, 'ALQ': 5, 'GLQ': 6},
                  'HeatingQC': {'NA': 0, 'Po': 1, 'Fa': 2, 'TA': 3, 'Gd': 4, 'Ex': 5},
                  'PoolQC':  {'NA': 0, 'Fa': 1, 'TA': 2, 'Gd': 3, 'Ex': 4},
                  'MasVnrType': {'BrkCmn': 1, 'BrkFace': 1, 'CBlock': 1, 'Stone': 1, 'None': 0},
                  'CentralAir': {'Y': 1,
                                 'N': 0},
                  'PavedDrive': {'Y': 1,
                                 'P': 0,
                                 'N': 0},
                  'Alley':{'No access': 0, 'Grvl': 1, 'Pave': 2 },
                  'Neighborhood': {
                      "MeadowV": 0,  # 88000
                      "IDOTRR": 1,  # 103000
                      "BrDale": 1,  # 106000
                      "OldTown": 1,  # 119000
                      "Edwards": 1,  # 119500
                      "BrkSide": 1,  # 124300
                      "Sawyer": 1,  # 135000
                      "Blueste": 1,  # 137500
                      "SWISU": 2,  # 139500
                      "NAmes": 2,  # 140000
                      "NPkVill": 2,  # 146000
                      "Mitchel": 2,  # 153500
                      "SawyerW": 2,  # 179900
                      "Gilbert": 2,  # 181000
                      "NWAmes": 2,  # 182900
                      "Blmngtn": 2,  # 191000
                      "CollgCr": 2,  # 197200
                      "ClearCr": 3,  # 200250
                      "Crawfor": 3,  # 200624
                      "Veenker": 3,  # 218000
                      "Somerst": 3,  # 225500
                      "Timber": 3,  # 228475
                      "StoneBr": 4,  # 278000
                      "NoRidge": 4,  # 290000
                      "NridgHt": 4,  # 315000
                  },
                  'SaleCondition': {'Abnorml': 1,
                                    'Alloca': 1,
                                    'AdjLand': 1,
                                    'Family': 1,
                                    'Normal': 0,
                                    'Partial': 0}
}


def clean_data(df):
    data = df.copy()
    data['LotFrontage'] = data['LotFrontage'].fillna(data['LotFrontage'].mean())

    data['Alley'] = data['Alley'].fillna('No access')

    data['MasVnrArea'] = data['MasVnrArea'].fillna(data['MasVnrArea'].mean())
    data['LotFrontage'] = data['LotFrontage'].fillna(data['LotFrontage'].mean())
    data['YearBuilt'] = 2017 - data['YearBuilt']
    data['YearRemodAdd'] = 2017 - data['YearRemodAdd']
    data['GarageYrBlt'] = 2017 - data['GarageYrBlt']
    data['GarageYrBlt'].fillna(data['GarageYrBlt'].mean(), inplace=True)
    data['YrSold'] = 2017 - data['YrSold']

    # GarageType, GarageFinish, GarageQual  NA in all. NA means No Garage
    for col in ('GarageType', 'GarageCond', 'GarageFinish', 'GarageQual'):
        data[col] = data[col].fillna('NoGarage')

    for k, v in REPLACE_VALUES.items():
        # print(k)
        data[k] = data[k].replace(v)
        data[k] = data[k].fillna(data[k].mean())

    return data


def pre_process_data(df_train, df_test):
    """
    Pre-process the data. fill the "NA" with "default". 
    Encode the categorical fields to numeric values.
    
    :return: preprocessed train, test DataFrames
    """
    print("Pre-processing the data...")
    for col in categorical_fields:
        df_train[col].fillna('default', inplace=True)
        df_test[col].fillna('default', inplace=True)

    for col in numerical_fields:
        df_train[col].fillna(df_train[col].mean(), inplace=True)
        df_test[col].fillna(df_test[col].mean(), inplace=True)

    # FIXME: adding noise here, instead of categorical feature introduce numerical
    encode = LabelEncoder()
    for col in categorical_fields:
        df_train[col] = encode.fit_transform(df_train[col])
        df_test[col] = encode.fit_transform(df_test[col])


    # loc[all_data.BsmtFinSF1.isnull(), 'BsmtFinSF1'] = all_data.BsmtFinSF1.median()
    return df_train, df_test


def scale_data(df_train, df_test):
    """
        Scale train test data with Standard scaler
    :param df_train: 
    :param df_test: 
    :return: 
    """

    scaler = StandardScaler()
    np_train = scaler.fit_transform(df_train)
    np_test = scaler.fit_transform(df_test)
    train_scaled = pd.DataFrame(data=np_train, index=df_train.index, columns=df_train.columns)
    test_scaled = pd.DataFrame(data=np_test, index=df_test.index, columns=df_test.columns)
    return train_scaled, test_scaled
