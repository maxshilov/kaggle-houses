import numpy as np
from sklearn.ensemble import RandomForestRegressor
from sklearn.model_selection import GridSearchCV
from .scorer import rmsle

""" best params: {'bootstrap': True,
 'criterion': 'mse',
 'max_depth': None,
 'max_features': 'auto',
 'max_leaf_nodes': None,
 'min_impurity_split': 1e-07,
 'min_samples_leaf': 1,
 'min_samples_split': 2,
 'min_weight_fraction_leaf': 0.0,
 'n_estimators': 300,
 'n_jobs': -1,
 'oob_score': False,
 'random_state': 42,
 'verbose': 0,
 'warm_start': False}
 """
# best score : -0.145871692


class RandomForest(object):
    def __init__(self, random_state):
        self.rnd = random_state
        self.best_model = None
        self.best_params = None

    def get_model(self):
        return RandomForestRegressor(n_jobs=-1, random_state=self.rnd)

    def get_param_grid(self):
        return {'n_estimators': np.arange(100, 500, 100), 'criterion': ['mse']}

    def grid_search(self, X, y, fold):
        grid = GridSearchCV(self.get_model(), self.get_param_grid(), scoring=rmsle, cv=fold, n_jobs=-1, verbose=True)
        grid.fit(X, y)
        print("Best estimator params : %s" % grid.best_params_)
        self.best_model = grid.best_estimator_
        self.best_params = grid.best_estimator_
        return self.best_model

    def predict(self, X_test):
        if self.best_model is None:
            raise Exception("Find best model with grid search first")

        return self.best_model.predict(X_test)
