import numpy as np
from sklearn.neighbors import KNeighborsRegressor
from sklearn.model_selection import GridSearchCV
from .scorer import rmsle

# mean crossval : -0.16765822094305274
# best params : {'metric': 'minkowski', 'n_neighbors': 5, 'p': 1}


class KNNRegression(object):

    def __init__(self, random_state=42):
        self.rnd = random_state
        self.model = None

    def get_model(self):
        return KNeighborsRegressor(n_jobs=-1)

    def get_params(self):
        return {'n_neighbors': np.arange(1, 12, 1), 'metric': ['euclidean', 'minkowski', 'chebyshev'], 'p': [1, 2]}

    def grid_search(self, X, y, folds):
        grid = GridSearchCV(self.get_model(), self.get_params(), scoring=rmsle, cv=folds, n_jobs=1, verbose=True)
        grid.fit(X, y)
        print("Best estimator params : %s" % grid.best_params_)
        return grid.best_estimator_
