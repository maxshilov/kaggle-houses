import xgboost as xgb
from hyperopt.pyll.base import scope
from sklearn.metrics import mean_squared_error
from hyperopt import hp
from hyperopt import fmin, tpe, hp, STATUS_OK, Trials
from sklearn.metrics import make_scorer


class XGBoostRegression(object):

    def __init__(self, X_train, y_train, X_valid, y_valid):
        self.X_train = X_train
        self.y_train = y_train
        self.X_valid = X_valid
        self.y_valid = y_valid
        self.best_model = None
        self.best_params = {}

    def get_model(self):
        if self.best_model is None:
            return xgb.XGBRegressor()
        return self.best_model

    def score(self, params):
        print("Training with params : ")
        print(params)

        model = xgb.XGBRegressor(**params)

        eval_set = [(self.X_train, self.y_train), (self.X_valid, self.y_valid)]
        model.fit(self.X_train, self.y_train, eval_set=eval_set, eval_metric='rmse', early_stopping_rounds=30)
        predictions = model.predict(self.X_valid)
        score = mean_squared_error(self.y_valid, predictions)
        print("\tScore {0}\n\n".format(score))

        return {'loss': score, 'status': STATUS_OK}

    def grid_search(self):
        trials = Trials()
        space = {
            'n_estimators': scope.int(hp.quniform('n_estimators', 100, 1000, 1)),
            'learning_rate': hp.quniform('learning_rate', 0.025, 0.5, 0.025),
            'max_depth': scope.int(hp.quniform('max_depth', 1, 13, 1)),
            'min_child_weight': hp.quniform('min_child_weight', 1, 6, 1),
            'subsample': hp.quniform('subsample', 0.5, 1, 0.05),
            'gamma': hp.quniform('gamma', 0.5, 1, 0.05),
            'objective': 'reg:linear',
            'silent': 1
        }
        #scorer = partialmethod(self.score)

        best = fmin(self.score, space, algo=tpe.suggest, trials=trials, max_evals=250)

        best['n_estimators'] = int(best['n_estimators'])
        best['min_child_weight'] = int(best['min_child_weight'])
        best['max_depth'] = int(best['max_depth'])
        self.best_params = best

        self.best_model = xgb.XGBRegressor()
        self.best_model.set_params(**best)
        print(best)
        return best

    def predict(self, X_test):
        if not self.best_params:
            self.grid_search()
            self.fit()

        return self.best_model.predict(X_test)

    def fit(self):
        if not self.best_model:
            self.grid_search()
        return self.best_model.fit(self.X_train, self.y_train)


