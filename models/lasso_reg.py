import numpy as np
from sklearn.linear_model import Lasso
from sklearn.model_selection import GridSearchCV
from .scorer import rmsle

"""
Lasso(alpha=425, copy_X=True, fit_intercept=True, max_iter=1000,
   normalize=False, positive=False, precompute=False, random_state=None,
   selection='cyclic', tol=0.01, warm_start=False)
"""


class LassoRegression(object):

    def __init__(self, random_state=42):
        self.rnd = random_state
        self.model = None

    def _get_model(self):
        return Lasso(random_state=self.rnd)

    def _get_param_grid(self):
        return {'alpha': np.arange(5, 1000, 25), 'tol': [0.001], 'max_iter': [1000]}

    def grid_search(self, X, y, fold):
        params = self._get_param_grid()
        grid = GridSearchCV(self._get_model(), params, scoring=rmsle, cv=fold, n_jobs=-1, verbose=True)
        grid.fit(X, y)
        print("Best estimator params : %s" % grid.best_params_)
        self.best_model = grid.best_estimator_

    def predict(self, X_test):
        return self.best_model.predict(X_test)
