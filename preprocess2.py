import numpy as np
import pandas as pd
from sklearn import preprocessing
from scipy.stats import skew


def clean_data(all_data):
    all_data = addFeatures(all_data)
    all_data = normalizeFeatures(all_data)
    all_data = categoricalToNumeric(all_data)
    all_data = dummiesFeatures(all_data)
    return all_data


def computeSkewness(dataset):
    numeric_feats = dataset.dtypes[dataset.dtypes != "object"].index
    skewed_feats = dataset[numeric_feats].apply(lambda x: skew(x.dropna()))  # compute skewness
    skewed_feats = skewed_feats[skewed_feats > 0.75]
    skewed_feats = skewed_feats.index

    dataset[skewed_feats] = np.log1p(dataset[skewed_feats])
    return dataset


def normalize(X):
    """Normalize an array, or a dataframe, to have mean 0 and stddev 1."""
    return (X - np.mean(X, axis=0)) / (np.std(X, axis=0))


def normalizeFeatures(dataset):
    dataset = dataset.fillna(dataset.mean())

    dataset["LotArea"] = normalize(dataset["LotArea"])
    dataset["YearBuilt"] = normalize(dataset["YearBuilt"])
    dataset["YearRemodAdd"] = normalize(dataset["YearRemodAdd"])
    dataset["PoolArea"] = normalize(dataset["PoolArea"])
    dataset["3SsnPorch"] = normalize(dataset["3SsnPorch"])
    dataset["YrSold"] = normalize(dataset["YrSold"])
    dataset["MoSold"] = normalize(dataset["MoSold"])

    dataset["MSSubClass"] = normalize(dataset["MSSubClass"])
    dataset["LotFrontage"] = normalize(dataset["LotFrontage"])
    dataset["MasVnrArea"] = normalize(dataset["MasVnrArea"])
    dataset["BsmtFinSF1"] = normalize(dataset["BsmtFinSF1"])
    dataset["BsmtFinSF2"] = normalize(dataset["BsmtFinSF2"])

    dataset["GrLivArea"] = normalize(dataset["GrLivArea"])
    dataset["GarageYrBlt"] = normalize(dataset["GarageYrBlt"])
    dataset["GarageCars"] = normalize(dataset["GarageCars"])
    dataset["GarageArea"] = normalize(dataset["GarageArea"])

    dataset["1stFlrSF"] = normalize(dataset["1stFlrSF"])
    dataset["2ndFlrSF"] = normalize(dataset["2ndFlrSF"])
    dataset["LowQualFinSF"] = normalize(dataset["LowQualFinSF"])
    dataset["GrLivArea"] = normalize(dataset["GrLivArea"])
    dataset["TotRmsAbvGrd"] = normalize(dataset["TotRmsAbvGrd"])
    dataset["GarageYrBlt"] = normalize(dataset["GarageYrBlt"])

    return dataset


def categoricalToNumeric(dataset):
    dataset = dataset.replace({
        "BsmtCond": {None: 0, "Po": 1, "Fa": 2, "TA": 3, "Gd": 4, "Ex": 5},
        "BsmtExposure": {None: 0, "Mn": 1, "Av": 2, "Gd": 3},
        "BsmtFinType1": {None: 0, "Unf": 1, "LwQ": 2, "Rec": 3, "BLQ": 4,
                         "ALQ": 5, "GLQ": 6},
        "BsmtFinType2": {None: 0, "Unf": 1, "LwQ": 2, "Rec": 3, "BLQ": 4,
                         "ALQ": 5, "GLQ": 6},
        "BsmtQual": {None: 0, "Po": 1, "Fa": 2, "TA": 3, "Gd": 4, "Ex": 5},
        "ExterCond": {"Po": 1, "Fa": 2, "TA": 3, "Gd": 4, "Ex": 5},
        "ExterQual": {"Po": 1, "Fa": 2, "TA": 3, "Gd": 4, "Ex": 5},
        "Functional": {None: 0, "Sal": 1, "Sev": 2, "Maj2": 3, "Maj1": 4, "Mod": 5,
                       "Min2": 6, "Min1": 7, "Typ": 8},
        "GarageCond": {None: 0, "Po": 1, "Fa": 2, "TA": 3, "Gd": 4, "Ex": 5},
        "GarageQual": {None: 0, "Po": 1, "Fa": 2, "TA": 3, "Gd": 4, "Ex": 5},
        "HeatingQC": {"Po": 1, "Fa": 2, "TA": 3, "Gd": 4, "Ex": 5},
        "KitchenQual": {None: 0, "Po": 1, "Fa": 2, "TA": 3, "Gd": 4, "Ex": 5},
        "LandSlope": {"Sev": 1, "Mod": 2, "Gtl": 3},
        "LotShape": {"IR3": 1, "IR2": 2, "IR1": 3, "Reg": 4},
        "PavedDrive": {"N": 0, "P": 1, "Y": 2},
        # "Utilities" : {"ELO" : 1, "NoSeWa" : 2, "NoSewr" : 3, "AllPub" : 4},
        "PoolQC": {None: 0, "Fa": 1, "TA": 2, "Gd": 3, "Ex": 4},
        "Alley": {None: 0, "Grvl": 1, "Pave": 2},
        "FireplaceQu": {None: 0, "Po": 1, "Fa": 2, "TA": 3, "Gd": 4, "Ex": 5},
    })

    return dataset


def dummiesFeatures(dataset):
    conv = pd.get_dummies(dataset['CentralAir'])
    dataset = pd.concat([conv, dataset], axis=1)
    conv = pd.get_dummies(dataset['Street'])
    dataset = pd.concat([conv, dataset], axis=1)
    conv = pd.get_dummies(dataset['RoofStyle'])
    dataset = pd.concat([conv, dataset], axis=1)
    conv = pd.get_dummies(dataset['SaleType'])
    dataset = pd.concat([conv, dataset], axis=1)
    conv = pd.get_dummies(dataset['SaleCondition'])
    dataset = pd.concat([conv, dataset], axis=1)
    conv = pd.get_dummies(dataset['LandContour'])
    dataset = pd.concat([conv, dataset], axis=1)
    conv = pd.get_dummies(dataset['LotConfig'])
    dataset = pd.concat([conv, dataset], axis=1)
    conv = pd.get_dummies(dataset['BldgType'])
    dataset = pd.concat([conv, dataset], axis=1)
    conv = pd.get_dummies(dataset['Fence'])
    dataset = pd.concat([conv, dataset], axis=1)
    conv = pd.get_dummies(dataset['Foundation'])
    dataset = pd.concat([conv, dataset], axis=1)
    conv = pd.get_dummies(dataset['MSZoning'])
    dataset = pd.concat([conv, dataset], axis=1)
    conv = pd.get_dummies(dataset['RoofMatl'])
    dataset = pd.concat([conv, dataset], axis=1)
    conv = pd.get_dummies(dataset['Exterior1st'])
    dataset = pd.concat([conv, dataset], axis=1)
    conv = pd.get_dummies(dataset['Exterior2nd'])
    dataset = pd.concat([conv, dataset], axis=1)
    conv = pd.get_dummies(dataset['MasVnrType'])
    dataset = pd.concat([conv, dataset], axis=1)
    conv = pd.get_dummies(dataset['HouseStyle'])
    dataset = pd.concat([conv, dataset], axis=1)
    conv = pd.get_dummies(dataset['Heating'])
    dataset = pd.concat([conv, dataset], axis=1)
    conv = pd.get_dummies(dataset['Electrical'])
    dataset = pd.concat([conv, dataset], axis=1)
    conv = pd.get_dummies(dataset['GarageType'])
    dataset = pd.concat([conv, dataset], axis=1)
    conv = pd.get_dummies(dataset['HouseStyle'])
    dataset = pd.concat([conv, dataset], axis=1)
    conv = pd.get_dummies(dataset['Neighborhood'])
    dataset = pd.concat([conv, dataset], axis=1)

    if "Id" in dataset:
        dataset.drop(['Id'], inplace=True, axis=1)

    return dataset


def addFeatures(dataset):
    # Proposed feature: '1stFlrSF' + '2ndFlrSF' to give us combined Floor Square Footage
    dataset['1stFlr_2ndFlr_Sf'] = np.log1p(dataset['1stFlrSF'] + dataset['2ndFlrSF'])
    # 1stflr+2ndflr+lowqualsf+GrLivArea = All_Liv_Area
    dataset['All_Liv_SF'] = np.log1p(dataset['1stFlr_2ndFlr_Sf'] + dataset['LowQualFinSF'] + dataset['GrLivArea'])

    dataset["IsRegularLotShape"] = (dataset["LotShape"] == "Reg") * 1
    # Most properties are level; bin the other possibilities together
    # as "not level".
    dataset["IsLandLevel"] = (dataset["LandContour"] == "Lvl") * 1
    # Most land slopes are gentle; treat the others as "not gentle".
    dataset["IsLandSlopeGentle"] = (dataset["LandSlope"] == "Gtl") * 1
    # Most properties use standard circuit breakers.
    dataset["IsElectricalSBrkr"] = (dataset["Electrical"] == "SBrkr") * 1
    # About 2/3rd have an attached garage.
    dataset["IsGarageDetached"] = (dataset["GarageType"] == "Detchd") * 1
    # Most have a paved drive. Treat dirt/gravel and partial pavement
    # as "not paved".
    dataset["IsPavedDrive"] = (dataset["PavedDrive"] == "Y") * 1
    # If YearRemodAdd != YearBuilt, then a remodeling took place at some point.
    dataset["Remodeled"] = (dataset["YearRemodAdd"] != dataset["YearBuilt"]) * 1
    # Did a remodeling happen in the year the house was sold?
    dataset["RecentRemodel"] = (dataset["YearRemodAdd"] == dataset["YrSold"]) * 1
    # Was this house sold in the year it was built?
    dataset["VeryNewHouse"] = (dataset["YearBuilt"] == dataset["YrSold"]) * 1
    dataset["Has2ndFloor"] = (dataset["2ndFlrSF"] == 0) * 1
    dataset["HasMasVnr"] = (dataset["MasVnrArea"] == 0) * 1
    dataset["HasWoodDeck"] = (dataset["WoodDeckSF"] == 0) * 1
    dataset["HasOpenPorch"] = (dataset["OpenPorchSF"] == 0) * 1
    dataset["HasEnclosedPorch"] = (dataset["EnclosedPorch"] == 0) * 1
    dataset["Has3SsnPorch"] = (dataset["3SsnPorch"] == 0) * 1
    dataset["HasScreenPorch"] = (dataset["ScreenPorch"] == 0) * 1

    dataset["SimplOverallQual"] = dataset.OverallQual.replace({1: 1, 2: 1, 3: 1,  # bad
                                                               4: 2, 5: 2, 6: 2,  # average
                                                               7: 3, 8: 3, 9: 3, 10: 3  # good
                                                               })
    dataset["SimplOverallCond"] = dataset.OverallCond.replace({1: 1, 2: 1, 3: 1,  # bad
                                                               4: 2, 5: 2, 6: 2,  # average
                                                               7: 3, 8: 3, 9: 3, 10: 3  # good
                                                               })
    dataset["SimplPoolQC"] = dataset.PoolQC.replace({1: 1, 2: 1,  # average
                                                     3: 2, 4: 2  # good
                                                     })
    dataset["SimplGarageCond"] = dataset.GarageCond.replace({1: 1,  # bad
                                                             2: 1, 3: 1,  # average
                                                             4: 2, 5: 2  # good
                                                             })
    dataset["SimplGarageQual"] = dataset.GarageQual.replace({1: 1,  # bad
                                                             2: 1, 3: 1,  # average
                                                             4: 2, 5: 2  # good
                                                             })
    dataset["SimplFireplaceQu"] = dataset.FireplaceQu.replace({1: 1,  # bad
                                                               2: 1, 3: 1,  # average
                                                               4: 2, 5: 2  # good
                                                               })
    dataset["SimplFireplaceQu"] = dataset.FireplaceQu.replace({1: 1,  # bad
                                                               2: 1, 3: 1,  # average
                                                               4: 2, 5: 2  # good
                                                               })
    dataset["SimplFunctional"] = dataset.Functional.replace({1: 1, 2: 1,  # bad
                                                             3: 2, 4: 2,  # major
                                                             5: 3, 6: 3, 7: 3,  # minor
                                                             8: 4  # typical
                                                             })
    dataset["SimplKitchenQual"] = dataset.KitchenQual.replace({1: 1,  # bad
                                                               2: 1, 3: 1,  # average
                                                               4: 2, 5: 2  # good
                                                               })
    dataset["SimplHeatingQC"] = dataset.HeatingQC.replace({1: 1,  # bad
                                                           2: 1, 3: 1,  # average
                                                           4: 2, 5: 2  # good
                                                           })
    dataset["BadHeating"] = dataset.HeatingQC.replace({1: 1,  # bad
                                                       2: 0, 3: 0,  # average
                                                       4: 0, 5: 0  # good
                                                       })
    dataset["SimplBsmtFinType1"] = dataset.BsmtFinType1.replace({1: 1,  # unfinished
                                                                 2: 1, 3: 1,  # rec room
                                                                 4: 2, 5: 2, 6: 2  # living quarters
                                                                 })
    dataset["SimplBsmtFinType2"] = dataset.BsmtFinType2.replace({1: 1,  # unfinished
                                                                 2: 1, 3: 1,  # rec room
                                                                 4: 2, 5: 2, 6: 2  # living quarters
                                                                 })
    dataset["SimplBsmtCond"] = dataset.BsmtCond.replace({1: 1,  # bad
                                                         2: 1, 3: 1,  # average
                                                         4: 2, 5: 2  # good
                                                         })
    dataset["SimplBsmtQual"] = dataset.BsmtQual.replace({1: 1,  # bad
                                                         2: 1, 3: 1,  # average
                                                         4: 2, 5: 2  # good
                                                         })
    dataset["SimplExterCond"] = dataset.ExterCond.replace({1: 1,  # bad
                                                           2: 1, 3: 1,  # average
                                                           4: 2, 5: 2  # good
                                                           })
    dataset["SimplExterQual"] = dataset.ExterQual.replace({1: 1,  # bad
                                                           2: 1, 3: 1,  # average
                                                           4: 2, 5: 2  # good
                                                           })
    return dataset













