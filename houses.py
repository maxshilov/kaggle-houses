from sklearn.model_selection import KFold, StratifiedKFold
from preprocess import pre_process_data, scale_data, clean_data
from sklearn.model_selection import train_test_split, validation_curve
import pandas as pd
import xgboost as xgb
from models.lasso_reg import LassoRegression
from models.xgboost import XGBoostRegression
from models.rand_forest import RandomForest
from models.knn import KNNRegression


RNG = 42


def main():
    print("Loading the data into Pandas Dataframe...")
    train = pd.read_csv("data/train.csv", index_col='Id')
    test = pd.read_csv("data/test.csv", index_col='Id')

    all_data = pd.concat((train.loc[:, 'MSSubClass':'SaleCondition'],
                          test.loc[:, 'MSSubClass':'SaleCondition']))

    all_data = clean_data(all_data)
    all_data = pd.get_dummies(all_data, dummy_na=True)
    all_data.fillna(all_data.mean(), inplace=True)

    df_train = all_data[:train.shape[0]].copy()
    df_test = all_data[train.shape[0]:].copy()
    y_train = train.SalePrice

    # Remove 2 outliers
    out_index = df_train[df_train["GrLivArea"] > 4600].index
    df_train.drop(out_index, inplace=True)
    y_train.drop(out_index, inplace=True)

    X_train, X_test = scale_data(df_train, df_test)

    #fold5 = KFold(X_train.shape[0], n_folds=5, shuffle=True, random_state=RNG)
    fold5 = StratifiedKFold(n_splits=5, shuffle=True, random_state=RNG)

    # lasso = LassoRegression(random_state=RNG)
    # lasso.grid_search(X_train, y_train, fold=fold5)
    # lasso_preds = lasso.predict(X_test)

    # X_train_xg, X_valid, y_train_xg, y_valid = train_test_split(X_train, y_train, test_size=0.2, random_state=1234)
    # xboost = XGBoostRegression(X_train_xg, y_train_xg, X_valid, y_valid)
    # xgb_preds = xboost.predict(X_test)

    rfr = RandomForest(random_state=RNG)
    rfr.grid_search(X_train, y_train, fold=fold5)
    rfr_preds = rfr.predict(X_test)


    # Blend the results of the two regressors and save the prediction to a CSV file.

    # y_pred = (y_pred_xgb + y_pred_lasso) / 2
    # y_pred = np.exp(y_pred)
    #
    # pred_df = pd.DataFrame(y_pred, index=test_df["Id"], columns=["SalePrice"])
    # pred_df.to_csv('output.csv', header=True, index_label='Id')


if __name__ == "__main__":
    main()
    print("Done!")
