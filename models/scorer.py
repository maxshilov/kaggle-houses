from sklearn.metrics import make_scorer
import numpy as np


def rmsle_func(ground_truth, predictions):
    try:
        n_preds, n_targets = predictions.shape
        p = predictions.sum(axis=1)
        a = ground_truth.sum(axis=1)
    except:
        n_preds = len(predictions)
        n_targets = 1
        p = predictions
        a = ground_truth

    sum_squared_error = np.sum((np.log(p + 1) - np.log(a + 1)) ** 2)
    return np.sqrt(1. / (n_preds * n_targets) * sum_squared_error)


rmsle = make_scorer(rmsle_func, greater_is_better=False)
